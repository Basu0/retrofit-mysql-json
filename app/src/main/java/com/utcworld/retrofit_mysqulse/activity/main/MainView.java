package com.utcworld.retrofit_mysqulse.activity.main;

import com.utcworld.retrofit_mysqulse.model.Note;

import java.util.List;

public interface MainView {
    void showLoading();
    void hideLoding();
    void onGetResult(List<Note>notes);
    void onErrorLoading(String message);
}
