package com.utcworld.retrofit_mysqulse.activity.editor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.thebluealliance.spectrum.SpectrumPalette;
import com.utcworld.retrofit_mysqulse.R;
import com.utcworld.retrofit_mysqulse.api.ApiClient;
import com.utcworld.retrofit_mysqulse.api.ApiInterface;
import com.utcworld.retrofit_mysqulse.model.Note;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditorActivity extends AppCompatActivity implements EditorView{
   EditText et_title,et_note;
   ProgressDialog progressDialog;
    ApiInterface apiInterface;
    SpectrumPalette palette;
    int color,id;
    String title,note;
    EditorPresenter presenter;
    Menu ActionMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        et_title=findViewById(R.id.title);
        et_note=findViewById(R.id.note);
        palette=findViewById(R.id.palette);



        palette.setOnColorSelectedListener(
                clr ->color=clr

        );
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Please wait...");

        presenter=new EditorPresenter(this);


        Intent intent=getIntent();
        id=intent.getIntExtra("id",0);
        title=intent.getStringExtra("title");
        note=intent.getStringExtra("notes");
        color=intent.getIntExtra("color",0);

        setDataFromIntentExtra();

    }

    private void setDataFromIntentExtra() {
        if (id!=0){
           et_title.setText(title);
            et_note.setText(note);
            palette.setBackgroundColor(color);
            getSupportActionBar().setTitle("Update Note");
            readModel();
        }
        else {
            editModel();
            palette.setSelectedColor(getResources().getColor(R.color.white));
            color=getResources().getColor(R.color.white);
        }
    }

    private void editModel() {
        et_title.setFocusableInTouchMode(true);
        et_note.setFocusableInTouchMode(true);
        palette.setEnabled(true);
    }

    private void readModel() {
        et_title.setFocusableInTouchMode(false);
        et_note.setFocusableInTouchMode(false);
        et_title.setFocusable(false);
        et_note.setFocusable(false);
        palette.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_edit,menu);
        ActionMenu=menu;
        if (id!=0){
            ActionMenu.findItem(R.id.edit).setVisible(true);
            ActionMenu.findItem(R.id.delete).setVisible(true);
            ActionMenu.findItem(R.id.update).setVisible(false);
            ActionMenu.findItem(R.id.save).setVisible(false);
        }
        else {

            ActionMenu.findItem(R.id.edit).setVisible(false);
            ActionMenu.findItem(R.id.delete).setVisible(false);
            ActionMenu.findItem(R.id.update).setVisible(true);
            ActionMenu.findItem(R.id.save).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String title=et_title.getText().toString().trim();
        String note=et_note.getText().toString().trim();
        int color=this.color;
        switch (item.getItemId()){
            case R.id.save:


                if (title.isEmpty()){
                    et_title.setError("Title is null");
                }
                if (note.isEmpty()){
                    et_note.setError("note is null");
                }
                else {
                    presenter.saveNote(title,note,color);
                }

            return true;
            case R.id.edit:
                editModel();
                ActionMenu.findItem(R.id.edit).setVisible(false);
                ActionMenu.findItem(R.id.delete).setVisible(false);
                ActionMenu.findItem(R.id.save).setVisible(false);
                ActionMenu.findItem(R.id.update).setVisible(true);
                return true;

            case R.id.update:
                if (title.isEmpty()){
                    et_title.setError("Title is null");
                }
                if (note.isEmpty()){
                    et_note.setError("note is null");
                }
                else {
                    presenter.update(id,title,note,color);
                }

                return true;
            case R.id.delete:
                AlertDialog alertbox = new AlertDialog.Builder(this)
                        .setMessage("Are you sure")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            // do something when the button is clicked
                            public void onClick(DialogInterface arg0, int arg1) {

                                presenter.deleteNote(id);
                                //close();


                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {

                            // do something when the button is clicked
                            public void onClick(DialogInterface arg0, int arg1) {
                            }
                        })
                        .show();




            default:
           return super.onOptionsItemSelected(item);
        }


    }


    @Override
    public void showProggress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
      progressDialog.hide();
    }

    @Override
    public void onRequestSuccess(String message) {
        Toast.makeText(this, "onRequestSuccess " +message, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();


    }

    @Override
    public void onRequestError(String message) {
        Toast.makeText(this, "onRequestError " +message, Toast.LENGTH_SHORT).show();
    }
}
