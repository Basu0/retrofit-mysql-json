package com.utcworld.retrofit_mysqulse.activity.editor;

public interface EditorView {

    void showProggress();
    void hideProgress();
    void onRequestSuccess(String message);
    void onRequestError(String message);
}
