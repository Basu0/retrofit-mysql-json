package com.utcworld.retrofit_mysqulse.activity.main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.utcworld.retrofit_mysqulse.R;
import com.utcworld.retrofit_mysqulse.activity.editor.EditorActivity;
import com.utcworld.retrofit_mysqulse.activity.editor.EditorView;
import com.utcworld.retrofit_mysqulse.model.Note;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {
    private static final int INTENT_EDIT =200;
    private static final int INTENT_ADD =100 ;
    FloatingActionButton  fab;
RecyclerView recyclerView;
SwipeRefreshLayout swipeRefresh;
MainPresenter Presenter;
 MainAdpator adpator;
 MainAdpator.ItemClickListener itemClickListener;
 List<Note> note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab=findViewById(R.id.add);
        recyclerView=findViewById(R.id.recycler_view);
        swipeRefresh=findViewById(R.id.swipe_refresh);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
       // LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
       // recyclerView.setLayoutManager(horizontalLayoutManager);
        fab.setOnClickListener(view->
                startActivityForResult(new Intent(this, EditorActivity.class),INTENT_ADD)

                );
        Presenter=new MainPresenter(this);
        Presenter.getData();
        swipeRefresh.setOnRefreshListener(
                ()->Presenter.getData()
        );
   itemClickListener=((view, postion) -> {
       int id=note.get(postion).getId();
         String title=note.get(postion).getTitle();
       String notes=note.get(postion).getNote();
       int color=note.get(postion).getColor();
       Intent intent=new Intent(this, EditorActivity.class);
       intent.putExtra("id", id);
       intent.putExtra("title", title);
       intent.putExtra("notes", notes);
       intent.putExtra("color", color);
       startActivityForResult(intent,INTENT_EDIT);

       //Toast.makeText(this, "Data "+title, Toast.LENGTH_SHORT).show();
       });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode==INTENT_ADD && requestCode==RESULT_OK){

            Presenter.getData();
        }else if (requestCode==INTENT_EDIT && requestCode==RESULT_OK){

            Presenter.getData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showLoading() {
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void hideLoding() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onGetResult(List<Note> notes) {
     adpator=new MainAdpator(this,notes,itemClickListener);
     adpator.notifyDataSetChanged();
     recyclerView.setAdapter(adpator);
     note=notes;
    }

    @Override
    public void onErrorLoading(String message) {
        Toast.makeText(this, "Load Data Error "+message, Toast.LENGTH_SHORT).show();
    }
}
