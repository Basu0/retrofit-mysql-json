package com.utcworld.retrofit_mysqulse.api;

import com.utcworld.retrofit_mysqulse.model.Note;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("save.php")
    Call<Note> saveNote(
            @Field("title") String title,
            @Field("note") String note,
            @Field("color") int color

    );

    @GET("notes.php")
    Call<List<Note>> getnote();

    @FormUrlEncoded
    @POST("update.php")
    Call<Note> updateNote(
            @Field("id") int id,
            @Field("title") String title,
            @Field("note") String note,
            @Field("color") int color

    );

    @FormUrlEncoded
    @POST("DeleteNote.php")
    Call<Note> DeleteNote(@Field("id") int id);
}
