package com.utcworld.retrofit_mysqulse.activity.main;

import androidx.annotation.NonNull;

import com.utcworld.retrofit_mysqulse.api.ApiClient;
import com.utcworld.retrofit_mysqulse.api.ApiInterface;
import com.utcworld.retrofit_mysqulse.model.Note;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {
   private MainView view;

    public MainPresenter(MainView view) {
        this.view = view;
    }

    void getData(){
        view.showLoading();
        //Request to server
        ApiInterface apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<Note>> call = apiInterface.getnote();
        call.enqueue(new Callback<List<Note>>() {
            @Override
            public void onResponse(@NonNull Call<List<Note>> call,@NonNull Response<List<Note>> response) {
               view.hideLoding();
               if (response.isSuccessful() && response.body() !=null){
                    view.onGetResult(response.body());

               }
            }

            @Override
            public void onFailure(@NonNull Call<List<Note>> call,@NonNull Throwable t) {
                view.hideLoding();
                view.onErrorLoading(t.getLocalizedMessage());
            }
        });
    }
}
